vim.g.mapleader = " "
vim.keymap.set("i", "<C-c>", "<Esc>")

-- shift lines
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- format code
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format)

-- prevent close
vim.api.nvim_set_keymap('n', '<C-z>', '<Nop>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('i', '<C-z>', '<Nop>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('v', '<RightMouse>', '<C-\\><C-g>gv<Cmd>:popup! PopUp<CR>', {})
vim.api.nvim_set_keymap('n', '<C-t>', ':NvimTreeToggle<CR>', {})

-- jump
vim.api.nvim_set_keymap('n', 'z', '<C-D>zz', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-Z>', '<C-U>zz', { noremap = true, silent = true })

-- redo
vim.api.nvim_set_keymap('n', '<C-U>', '<C-R>', { silent = true })
vim.api.nvim_set_keymap('i', '<C-U>', '<C-R>', { silent = true })

-- pane
vim.api.nvim_set_keymap('n', '<C-l>', '<C-W>w', { noremap = true, silent = true })

-- jump per word
vim.api.nvim_set_keymap('n', '<C-W>', 'b', { silent = true })
vim.api.nvim_set_keymap('v', '<C-W>', 'b', { silent = true })


